# Wymagania funkcjonalne

[V] 1. Skrypt powinien pobierać jeden adres ip:
  a. albo za pomocą argumentu,
  b. jeżeli brakuje IP jako argumentu, powinien pobrać go za pomocą standardowego wejścia.

[V] 2. Jeżeli podany zostanie argument '-h' lub '–help' powinien wyświetlić informacje o pomocy.

[V] 3. Jeżeli podanie zostany argument '-c' lib '–csv' format wyświetlania powinien być CSV (czyli w jednym wierszu oddzielone średnikami - patrz
przykład u góry).

[V] 4. Dla każdego podanego adresu IP powinien wyświetlić 3 informacje: Lokalizacja (miasto), Organizacja, ASN.

[V] 5. Skrypt powinien sprawdzać, czy podany adres IP jest prawidłowy i ew. wyświetlać stosowny komunikat błędu, jeżeli nie jest poprawny.

# Wymagania niefunkcjonalne

[V] 1. Kod wystawiony jako publiczne repozytorium na GitLab.

[V]2. Dodatkowo należy stworzyć obraz Docker oraz udostępnić go wybranym repozytorium obrazów docker (DockerHub, GitLab), który zawiera skrypt
jak i zależności aby go uruchomić.

[X]3. Należy zautomatyzować budowanie obrazu Docker i wysyłanie go na wybrane repozytorium, używając GitLabCI.

[X]4. GitLab CI powinien mieć dwa "stage" (stany):
 a. build - budujący obraz dockerowy z narzędziem i zależnościami;
 b. test - testujący, że obraz działa - wywołujący narzędzie dla adresu IP: 185.33.37.131 tak aby wyświetliło informacje o tym adresie
 pokazując, że obraz/narzędzie działa.
